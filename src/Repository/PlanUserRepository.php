<?php

namespace App\Repository;

use App\Entity\PlanUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlanUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanUser[]    findAll()
 * @method PlanUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlanUser::class);
    }

    public function getPlanUserGridDataByPlanId($planId){
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                    SELECT pu.*,concat(u.first_name, \' \',u.last_name) username FROM plan_user pu, user u, plan p  
                    WHERE  pu.plan_id = '.$planId.' and pu.plan_id = p.id and pu.user_id = u.id
                    ORDER BY u.first_name,u.last_name
                ';

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        return $stmt->fetchAll();

        return $data;            
    }

    // /**
    //  * @return PlanUser[] Returns an array of PlanUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlanUser
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
