<?php

namespace App\Repository;

use App\Entity\PlanDifficulty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlanDifficulty|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanDifficulty|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanDifficulty[]    findAll()
 * @method PlanDifficulty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanDifficultyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlanDifficulty::class);
    }

    // /**
    //  * @return PlanDifficulty[] Returns an array of PlanDifficulty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlanDifficulty
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
