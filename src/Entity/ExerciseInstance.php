<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExerciseInstanceRepository")
 */
class ExerciseInstance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
     /**
     * @ORM\Column(type="integer")
     */
    private $DayId;

    /**
     * @ORM\Column(type="integer")
     */
    private $ExerciseId;

    /**
     * @ORM\Column(type="integer")
     */
    private $ExerciseDuration;

    /**
     * @ORM\Column(type="integer")
     */
    private $ExerciseOrder;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->id = $Id;

        return $this;
    }

     public function getDayId(): ?int
    {
        return $this->DayId;
    }

    public function setDayId(int $Id): self
    {
        $this->DayId = $Id;

        return $this;
    }

    public function getExerciseId(): ?int
    {
        return $this->ExerciseId;
    }

    public function setExerciseId(int $Name): self
    {
        $this->ExerciseId = $Name;

        return $this;
    }

    public function getExerciseDuration(): ?int
    {
        return $this->ExerciseDuration;
    }

    public function setExerciseDuration(int $ExerciseDuration): self
    {
        $this->ExerciseDuration = $ExerciseDuration;

        return $this;
    }

    public function getExerciseOrder(): ?int
    {
        return $this->ExerciseOrder;
    }

    public function setExerciseOrder(int $ExerciseOrder): self
    {
        $this->ExerciseOrder = $ExerciseOrder;

        return $this;
    }

}
