<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PlanName;

    /**
     * @ORM\Column(type="string", length=4000)
     */
    private $PlanDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $PlanDifficulty;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->id = $Id;

        return $this;
    }

    public function getPlanName(): ?string
    {
        return $this->PlanName;
    }

    public function setPlanName(string $Name): self
    {
        $this->PlanName = $Name;

        return $this;
    }

    public function getPlanDescription(): ?string
    {
        return $this->PlanDescription;
    }

    public function setPlanDescription(string $Description): self
    {
        $this->PlanDescription = $Description;

        return $this;
    }

    public function getPlanDifficulty(): ?int
    {
        return $this->PlanDifficulty;
    }

    public function setPlanDifficulty(string $Difficulty): self
    {
        $this->PlanDifficulty = $Difficulty;

        return $this;
    }

}
