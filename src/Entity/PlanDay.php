<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanDayRepository")
 */
class PlanDay
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
     /**
     * @ORM\Column(type="integer")
     */
    private $PlanId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $DayName;

    /**
     * @ORM\Column(type="integer")
     */
    private $DayOrder;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->id = $Id;

        return $this;
    }

     public function getPlanId(): ?int
    {
        return $this->PlanId;
    }

    public function setPlanId(int $Id): self
    {
        $this->PlanId = $Id;

        return $this;
    }

    public function getDayName(): ?string
    {
        return $this->DayName;
    }

    public function setDayName(string $Name): self
    {
        $this->DayName = $Name;

        return $this;
    }

    public function getDayOrder(): ?int
    {
        return $this->DayOrder;
    }

    public function setDayOrder(int $Order): self
    {
        $this->DayOrder = $Order;

        return $this;
    }

}
