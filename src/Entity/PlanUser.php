<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanUserRepository")
 */
class PlanUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
     /**
     * @ORM\Column(type="integer")
     */
    private $PlanId;

    /**
     * @ORM\Column(type="integer")
     */
    private $UserId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->id = $Id;

        return $this;
    }

     public function getPlanId(): ?int
    {
        return $this->PlanId;
    }

    public function setPlanId(int $Id): self
    {
        $this->PlanId = $Id;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->UserId;
    }

    public function setUserId(int $Id): self
    {
        $this->UserId = $Id;

        return $this;
    }
}
