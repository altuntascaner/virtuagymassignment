<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\PlanDay;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlanDayController extends AbstractController
{
    public function index()
    {
    }

    public function getPlanDayGridDataByPlanId($planId){

        $res = "";

        try{

            $planDays = $this->getDoctrine()->getRepository(PlanDay::class)
                                            ->findBy(["PlanId" => $planId],["DayOrder" => "ASC"] );  

            $res = $this->getGridHtml($planDays);
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    private function getGridHtml($planDays){
        $res = "";

        if(count($planDays) > 0){

                foreach ($planDays as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataPlanDayId" style="display:none">'.$item->getId().'</td>'.
                      '<td class="rowDataPlanDayName" >'.$item->getDayName().'</td>'.                      
                      '<td class="rowDataPlanOrder">'.$item->getDayOrder().'</td>'.
                      '<td><button type="button" class="editPlanDayItem btn btn-primary" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removePlanDayItem btn btn-danger" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="showDetailPlanDayItem btn btn-info" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
            }   
        }
        else{
            $res = "<tr>".    
                        "<td>No record exists.</td>".
                    "</tr>";    
        }

        return $res;    
    }

    public function savePlanDayItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $planDayJson = $request->request->get('planDay');

            $newPlanDay = $serializer->deserialize($planDayJson, PlanDay::class, 'json');//create object


            $entityManager = $this->getDoctrine()->getManager();
            
            $planDayToSave;            

            $message = "";

            if($newPlanDay->getId() == ""){
                $planDayToSave = $newPlanDay;
                $message = "A new exercise day ('".$newPlanDay->getDayName()."'') is added to your exercise plan.";
            }
            else{
                $planDayToSave = $entityManager->getRepository(PlanDay::class)
                                                ->find($newPlanDay->getId());

                $planDayToSave->setDayName($newPlanDay->getDayName());
                $planDayToSave->setDayOrder($newPlanDay->getDayOrder());

                $message = "The exercise day ('".$newPlanDay->getDayName()."'') is updated in your exercise plan.";
            }
                
            $entityManager->persist($planDayToSave);//save planDay
            $entityManager->flush();

            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                    ["message"=>$message,"planId"=>$planDayToSave->getPlanId()]);
            

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removePlanDayItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $planDay = $entityManager->getRepository(PlanDay::class)
                                                ->find($id);                               
                                                    
            $entityManager->remove($planDay);//remove planDay
            $entityManager->flush(); 
            
             $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                        ["message"=>"The exercise day ('".$planDay->getDayName()."'') is removed from your exercise plan.","planId"=>$planDay->getPlanId()]);
           

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
