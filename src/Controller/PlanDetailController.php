<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Plan;
use App\Entity\Exercise;
use App\Entity\User;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlanDetailController extends AbstractController
{
    public function index()
    {
        
    }

    public Function getPlanDetail($id){
        
        $res = [];

        try 
        {            
            $entityManager = $this->getDoctrine();

            $plan = $entityManager->getRepository(Plan::class)
                                  ->find($id);

            $res["plan"] = $plan;  

            $exercises = $entityManager->getRepository(Exercise::class)
                                        ->findAll();

            $res["exercises"] = $exercises;

            $users = $entityManager->getRepository(User::class)
                                        ->findAll();

            $res["users"] = $users;                            

        } catch (Exception $e) {
            
        } 

        return $this->render('PlanDetail/index.html.twig',["model"=>$res]);
    }

    public function getPlanGridData(){

        $res = "";

        try{

            $plans = $this->getDoctrine()->getRepository(Plan::class)
                                        ->getPlanGridData();  
                                        
            if(count($plans) > 0){

                foreach ($plans as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataPlanId" style="display:none">'.$item['id'].'</td>'.
                      '<td class="rowDataPlanName">'.$item['plan_name'].'</td>'.
                      '<td>'.
                            '<textarea readonly="true" class="rowDataPlanDescription textareaPlanDescription" >'.$item['plan_description'].'</textarea>'.
                      '</td>'.
                      '<td class="rowDataPlanDifficulty" style="display:none">'.$item['plan_difficulty'].'</td>'.
                      '<td>'.$item['difficulty_name'].'</td>'.
                      '<td><button type="button" class="editPlanItem btn btn-primary" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removePlanItem btn btn-danger" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
                }   
            }
            else{
                $res = "<tr>".    
                            "<td>No record exists.</td>".
                        "</tr>";    
            }
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    public function savePlanItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $planJson = $request->request->get('plan');

            $newPlan = $serializer->deserialize($planJson, Plan::class, 'json');//create object

            $entityManager = $this->getDoctrine()->getManager();
            
            $planToSave;            

            if($newPlan->getId() == ""){
                $planToSave = $newPlan;
            }
            else{
                $planToSave = $entityManager->getRepository(Plan::class)
                                                ->find($newPlan->getId());

                $planToSave->setPlanName($newPlan->getPlanName());
                $planToSave->setPlanDescription($newPlan->getPlanDescription());                                    
                $planToSave->setPlanDifficulty($newPlan->getPlanDifficulty());                                    
                                                    
            }
                
            $entityManager->persist($planToSave);//save plan
            $entityManager->flush();

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removePlanItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $plan = $entityManager->getRepository(Plan::class)
                                                ->find($id);                               
                                                    
            $entityManager->remove($plan);//remove plan
            $entityManager->flush(); 

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
