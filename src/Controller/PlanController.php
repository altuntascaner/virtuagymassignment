<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Plan;
use App\Entity\PlanDifficulty;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlanController extends AbstractController
{

    public function index()
    {
        $planDifficulties = $this->getDoctrine()
                                 ->getRepository(PlanDifficulty::class)
                                 ->findAll();

        return $this->render('Plan/index.html.twig',["planDifficulties"=>$planDifficulties]);
    }

    public function getPlanGridData(){

        $res = "";

        try{

            $plans = $this->getDoctrine()->getRepository(Plan::class)
                                        ->getPlanGridData();  
                                        
            if(count($plans) > 0){

                foreach ($plans as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataPlanId" style="display:none">'.$item['id'].'</td>'.
                      '<td class="rowDataPlanName" width="40%" >'.$item['plan_name'].'</td>'.
                      '<td>'.
                            '<textarea readonly="true" class="rowDataPlanDescription textareaPlanDescription" >'.$item['plan_description'].'</textarea>'.
                      '</td>'.
                      '<td class="rowDataPlanDifficulty" style="display:none">'.$item['plan_difficulty'].'</td>'.
                      '<td>'.$item['difficulty_name'].'</td>'.
                      '<td><button type="button" class="editPlanItem btn btn-primary" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removePlanItem btn btn-danger" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="showDetailPlanItem btn btn-info" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
                }   
            }
            else{
                $res = "<tr>".    
                            "<td>No record exists.</td>".
                        "</tr>";    
            }
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    public function savePlanItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $planJson = $request->request->get('plan');

            $newPlan = $serializer->deserialize($planJson, Plan::class, 'json');//create object

            $entityManager = $this->getDoctrine()->getManager();
            
            $planToSave;            

            if($newPlan->getId() == ""){
                $planToSave = $newPlan;
            }
            else{
                $planToSave = $entityManager->getRepository(Plan::class)
                                                ->find($newPlan->getId());

                $planToSave->setPlanName($newPlan->getPlanName());
                $planToSave->setPlanDescription($newPlan->getPlanDescription());                                    
                $planToSave->setPlanDifficulty($newPlan->getPlanDifficulty());
            }
                
            $entityManager->persist($planToSave);//save plan
            $entityManager->flush();
            
            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                    ["message"=>'Your exercise plan \''.$newPlan->getPlanName().'\' is updated.',"planId"=>$planToSave->getId()]);
                     
            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removePlanItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $plan = $entityManager->getRepository(Plan::class)
                                                ->find($id);                               
                                                    
            $entityManager->remove($plan);//remove plan
            $entityManager->flush(); 

            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                    ["message"=>'Your exercise plan \''.$plan->getPlanName().'\' is deleted.',"planId"=>$plan->getId()]);

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
