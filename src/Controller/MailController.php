<?php

namespace App\Controller;

use App\Entity\Plan;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MailController extends AbstractController
{
    public function index()
    {        
    }

    public function sendMailToRelatedUsers($message,$planId,$toList=null, \Swift_Mailer $mailer){
        try {

            if($toList != null){
                $to = $toList;
                $planName = $this->getDoctrine()
                                ->getRepository(Plan::class)
                                ->find($planId)
                                ->getPlanName();    
            }
            else{

                $conn = $this->getDoctrine()->getConnection();

                $sql = '
                            SELECT u.email,p.plan_name  FROM plan_user pu, user u, plan p 
                            WHERE pu.plan_id = \''.$planId.'\' and pu.user_id = u.id and pu.plan_id = p.id
                        ';

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                
                $userMails = $stmt->fetchAll();

                $planName = "";

                if(count($userMails) > 0){
                    
                    $planName = $userMails[0]['plan_name'];

                    $to = array();

                    foreach($userMails as $mail){
                        $to[] = $mail['email'];
                    }
                }
            }

             $message = (new \Swift_Message('About Your Exercise Plan'))
                ->setFrom('noreply@gmail.com')
                ->setTo($to)
                ->setBody(
                    $this->renderView(
                        'Mail/index.html.twig',
                        ['message' => $message.' Plan name : '.$planName]
                    ),
                    'text/html'
            );

            $mailer->send($message);

        } catch (Exception $e) {
                    
        }
        return new Response();
    }
    
}
