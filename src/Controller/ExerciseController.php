<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Exercise;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ExerciseController extends AbstractController
{
    public function index()
    {
        return $this->render('Exercise/index.html.twig');
    }

    public function getExerciseGridData(){

        $res = "";

        try{

            $exercises = $this->getDoctrine()->getRepository(Exercise::class)
                                        ->findAll();  
                                        
            if(count($exercises) > 0){

                foreach ($exercises as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataExerciseId" style="display:none">'.$item->getId().'</td>'.
                      '<td class="rowDataExerciseName" >'.$item->getExerciseName().'</td>'.
                      '<td><button type="button" class="editExerciseItem btn btn-primary" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removeExerciseItem btn btn-danger" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
                }   
            }
            else{
                $res = "<tr>".    
                            "<td>No record exists.</td>".
                        "</tr>";    
            }
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    public function saveExerciseItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $exerciseJson = $request->request->get('exercise');

            $newExercise = $serializer->deserialize($exerciseJson, Exercise::class, 'json');//create object

            $entityManager = $this->getDoctrine()->getManager();
            
            $exerciseToSave;            

            if($newExercise->getId() == ""){
                $exerciseToSave = $newExercise;
            }
            else{
                $exerciseToSave = $entityManager->getRepository(Exercise::class)
                                                ->find($newExercise->getId());

                $exerciseToSave->setExerciseName($newExercise->getExerciseName());                            
                                                    
            }
                
            $entityManager->persist($exerciseToSave);//save exercise
            $entityManager->flush();

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removeExerciseItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $exercise = $entityManager->getRepository(Exercise::class)
                                                ->find($id);                               
                                                    
            $entityManager->remove($exercise);//remove exercise
            $entityManager->flush(); 

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
