<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserController extends AbstractController
{
    public function index()
    {
        return $this->render('User/index.html.twig');
    }

    public function getUserGridData(){

        $res = "";

        try{

            $users = $this->getDoctrine()->getRepository(User::class)
                                        ->findAll();  
                                        
            if(count($users) > 0){

                foreach ($users as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataUserId" style="display:none">'.$item->getId().'</td>'.
                      '<td class="rowDataUserFirstName" >'.$item->getFirstName().'</td>'.
                      '<td class="rowDataUserLastName" >'.$item->getLastName().'</td>'.
                      '<td class="rowDataUserEmail">'.$item->getEMail().'</td>'.
                      '<td><button type="button" class="editUserItem btn btn-primary" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removeUserItem btn btn-danger" itemid="'.$item->getId().'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
                }   
            }
            else{
                $res = "<tr>".    
                            "<td>No record exists.</td>".
                        "</tr>";    
            }
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    public function saveUserItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $userJson = $request->request->get('user');

            $newUser = $serializer->deserialize($userJson, User::class, 'json');//create object

            $entityManager = $this->getDoctrine()->getManager();
            
            $userToSave;            

            if($newUser->getId() == ""){
                $userToSave = $newUser;
            }
            else{
                $userToSave = $entityManager->getRepository(User::class)
                                                ->find($newUser->getId());

                $userToSave->setFirstName($newUser->getFirstName());
                $userToSave->setLastName($newUser->getLastName());                                    
                $userToSave->setEmail($newUser->getEmail());                                    
                                                    
            }
                
            $entityManager->persist($userToSave);//save user
            $entityManager->flush();

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removeUserItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager->getRepository(User::class)
                                                ->find($id);                               
                                                    
            $entityManager->remove($user);//remove user
            $entityManager->flush(); 

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
