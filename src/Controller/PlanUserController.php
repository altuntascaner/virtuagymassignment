<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\PlanUser;
use App\Entity\User;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlanUserController extends AbstractController
{
    public function index()
    {
    }

    public function getPlanUserGridDataByPlanId($planId){

        $res = "";

        try{

            $planUsers = $this->getDoctrine()->getRepository(PlanUser::class)
                                            ->getPlanUserGridDataByPlanId($planId);  

            $res = $this->getGridHtml($planUsers);
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    private function getGridHtml($planUsers){
        $res = "";

        if(count($planUsers) > 0){

                foreach ($planUsers as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataPlanUserId" style="display:none">'.$item['id'].'</td>'.
                      '<td class="rowDataPlanUserName" >'.$item['username'].'</td>'.
                      '<td><button type="button" class="removePlanUserItem btn btn-danger" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
            }   
        }
        else{
            $res = "<tr>".    
                        "<td>No record exists.</td>".
                    "</tr>";    
        }

        return $res;    
    }

    public function savePlanUserItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $planUserJson = $request->request->get('planUser');

            $newPlanUser = $serializer->deserialize($planUserJson, PlanUser::class, 'json');//create object


            $entityManager = $this->getDoctrine()->getManager();                      
                
            $entityManager->persist($newPlanUser);//save planUser
            $entityManager->flush();


            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                        ["message"=>"A new exercise plan is assigned to you.","planId"=>$newPlanUser->getPlanId()]);
           

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removePlanUserItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $planUser = $entityManager->getRepository(PlanUser::class)
                                                ->find($id);                               
            

            $entityManager->remove($planUser);//remove planUser
            $entityManager->flush(); 

            $email = $entityManager->getRepository(User::class)
                                                ->find($planUser->getUserId())
                                                ->getEMail();

            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                        ["message"=>" You are unassigned from the exercise plan.","planId"=>$planUser->getPlanId(),"toList"=>$email]);

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
