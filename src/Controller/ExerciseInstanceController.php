<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\ExerciseInstance;
use App\Entity\Exercise;
use App\Entity\PlanDay;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ExerciseInstanceController extends AbstractController
{
    public function index()
    {
    }

    public function getExerciseInstanceGridDataByDayId($dayId){

        $res = "";

        try{

            $instances = $this->getDoctrine()->getRepository(ExerciseInstance::class)
                                            ->getExerciseInstanceGridDataByDayId($dayId);  

            $res = $this->getGridHtml($instances);
        }
        catch(Exception $e){
            $res = "error";
        }

        return new Response($res);
    }

    private function getGridHtml($instances){
        $res = "";

        if(count($instances) > 0){

                foreach ($instances as $item) {

                    $res .= '<tr>'.
                      '<td class="rowDataExerciseInstanceId" style="display:none">'.$item['id'].'</td>'.
                      '<td class="rowDataExerciseInstanceExercise" >'.$item['exercise_name'].'</td>'.
                      '<td class="rowDataExerciseInstanceDuration" >'.$item['exercise_duration'].'</td>'.                   
                      '<td class="rowDataExerciseInstanceOrder">'.$item['exercise_order'].'</td>'.
                      '<td class="rowDataExerciseInstanceExerciseId" style="display:none">'.$item['exercise_id'].'</td>'.
                      '<td><button type="button" class="editExerciseInstanceItem btn btn-primary" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'.
                      '<td><button type="button" class="removeExerciseInstanceItem btn btn-danger" itemid="'.$item['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>'.
                    '</tr>';                              
            }   
        }
        else{
            $res = "<tr>".    
                        "<td>No record exists.</td>".
                    "</tr>";    
        }

        return $res;    
    }

    public function saveExerciseInstanceItem(Request $request){
        try {            
        
            $encoders = array(new JsonEncoder()); // to deserialize form data got as json
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $exerciseInstance = $request->request->get('exerciseInstance');

            $newExerciseInstance = $serializer->deserialize($exerciseInstance, ExerciseInstance::class, 'json');//create object

            $entityManager = $this->getDoctrine()->getManager();
            
            $planDay = $entityManager->getRepository(PlanDay::class)
                            ->find($newExerciseInstance->getDayId());

            $planId  = $planDay->getPlanId();
            $dayName = $planDay->getDayName();

            $exerciseName = $entityManager->getRepository(Exercise::class)
                            ->find($newExerciseInstance->getExerciseId())
                            ->getExerciseName();

            
            
            $exerciseInstanceToSave;  

            $message = "";

            if($newExerciseInstance->getId() == ""){
                $exerciseInstanceToSave = $newExerciseInstance;

                $message = "A new exercise ('".$exerciseName."') is added to your exercise plan day ('".$dayName."').";
            }
            else{
                $exerciseInstanceToSave = $entityManager->getRepository(ExerciseInstance::class)
                                                ->find($newExerciseInstance->getId());

                $exerciseInstanceToSave->setExerciseId($newExerciseInstance->getExerciseId());
                $exerciseInstanceToSave->setExerciseDuration($newExerciseInstance->getExerciseDuration());
                $exerciseInstanceToSave->setExerciseOrder($newExerciseInstance->getExerciseOrder());

                $message = "The exercise ('".$exerciseName."') is updated on your exercise plan day ('".$dayName."').";
            }

            $entityManager->persist($exerciseInstanceToSave);//save
            $entityManager->flush();

            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                    ["message"=>$message,"planId"=>$planId]);

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }

    public function removeExerciseInstanceItem($id){
        try { 
            $entityManager = $this->getDoctrine()->getManager();

            $exerciseInstance = $entityManager->getRepository(ExerciseInstance::class)
                                                ->find($id);                               
            

            $planDay = $entityManager->getRepository(PlanDay::class)
                            ->find($exerciseInstance->getDayId());

            $planId  = $planDay->getPlanId();
            $dayName = $planDay->getDayName();

            $exerciseName = $entityManager->getRepository(Exercise::class)
                            ->find($exerciseInstance->getExerciseId())
                            ->getExerciseName(); 


            $entityManager->remove($exerciseInstance);//remove planDay
            $entityManager->flush(); 


            $this->forward('App\Controller\MailController:sendMailToRelatedUsers',
                    ["message"=>"The exercise ('".$exerciseName."') is removed from your exercise plan day ('".$dayName."').","planId"=>$planId]);

            return new Response("success");

        } catch (Exception $e) {
            return new Response("error");
        }
    }
    
}
