


$(document).on('click','#addPlanDayItem',function(){

	clearPlanDayItemInfoModal();
	
	$('#planDayItemInfoModal .modal-title').html('Add New Plan Day');

	$('#planDayItemInfoModal').modal('show');
});

$(document).on('click','.editPlanDayItem',function(){

	clearPlanDayItemInfoModal();
	
	var tr = $(this).closest('tr');

	setPlanDayItemInfoModalObject(tr);

	$('#planDayItemInfoModal .modal-title').html('Edit Plan Day');

	$('#planDayItemInfoModal').modal('show');
});

$(document).on('click','.removePlanDayItem',function(){
	
	var tr = $(this).closest('tr');
	var dayId = tr.find('.rowDataPlanDayId').html();
	var planId = $('#currentPlanId').val();

    openGeneralRemoveConfirmModal('/planDay/remove/'+dayId,
    	'getPlanDayGridData(\''+planId+'\');');
});

$(document).on('click','.showDetailPlanDayItem',function(){
	
	var tr = $(this).closest('tr');
	var dayId = tr.find('.rowDataPlanDayId').html();
	var dayName = tr.find('.rowDataPlanDayName').html();

	$('#currentPlanDayId').val(dayId);
	$('#panelPlanDayExercises .panel-heading span:eq(0)').html(dayName+' Exercises');
	$('#addExerciseInstanceItem').show();

    getExerciseInstanceGridData(dayId);
});

$('#planDayItemSave').on('click',function(){
	var planDayItemObject = getPlanDayItemInfoObject();

	if (planDayItemObject['DayName'] == ''){
		openNotify('warning','Please fill all required (*) information');
	}
	else if(planDayItemObject['DayOrder'] == '' || !isPositiveNumber(planDayItemObject['DayOrder']) || parseInt(planDayItemObject['DayOrder']) > 999999999){
		openNotify('warning','\'Order\' is out of limit.');
	}
	else{
		$.ajax(
			{  
	           url: '/planDay/save',  
	           type: 'POST',
	           data: {planDay : JSON.stringify(planDayItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getPlanDayGridData(planDayItemObject['PlanId']);

	           			openNotify('success','Save operation succeeded.');

	           			$('#planDayItemInfoModal').modal('hide');	
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getPlanDayGridData(id){
	$('#currentPlanDayId').val('');
	$('#panelPlanDayExercises .panel-heading span:eq(0)').html('Exercises');
	$('#tableExerciseInstances tbody').empty();

	$('#planDayItemInfoModal').modal('hide');
	$('#addExerciseInstanceItem').hide();
		
	$.ajax(
		{  
           url: '/planDay/getPlanDayGridData/'+id,  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tablePlanDays tbody').html(result);           		
           }  
       }
    );
}

function clearPlanDayItemInfoModal(){
	$('#planDayIdPlanDayItemInfo').val('');
	$('#planDayDayNamePlanDayItemInfo').val('');
	$('#planDayOrderPlanDayItemInfo').val('');
}

function getPlanDayItemInfoObject(){
	var obj =  {
		PlanId : $('#planDayPlanIdPlanDayItemInfo').val(),		
		DayName : $('#planDayDayNamePlanDayItemInfo').val().trim(),
		DayOrder : $('#planDayOrderPlanDayItemInfo').val().trim()
	};

	var id = $('#planDayIdPlanDayItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}

function setPlanDayItemInfoModalObject(tr){
	$('#planDayIdPlanDayItemInfo').val(tr.find('.rowDataPlanDayId').html());
	$('#planDayDayNamePlanDayItemInfo').val(tr.find('.rowDataPlanDayName').html());
	$('#planDayOrderPlanDayItemInfo').val(tr.find('.rowDataPlanOrder').html());	
}


