
getPlanGridData();

$(document).on('click','#addPlanItem',function(){

	clearPlanItemInfoModal();
	
	$('#planItemInfoModal .modal-title').html('Add New Plan');

	$('#planItemInfoModal').modal('show');
});

$(document).on('click','.editPlanItem',function(){

	clearPlanItemInfoModal();
	
	var tr = $(this).closest('tr');

	setPlanItemInfoModalObject(tr);

	$('#planItemInfoModal .modal-title').html('Edit Plan');

	$('#planItemInfoModal').modal('show');
});

$(document).on('click','.removePlanItem',function(){
	
	var tr = $(this).closest('tr');
	var id = tr.find('td:eq(0)').html();

    openGeneralRemoveConfirmModal('/plan/remove/'+id,'getPlanGridData()');
});

$(document).on('click','.showDetailPlanItem',function(){
	
	var tr = $(this).closest('tr');
	var id = tr.find('td:eq(0)').html();

    window.open('/planDetail/'+id,'_blank');
});

$('#planItemSave').on('click',function(){
	var planItemObject = getPlanItemInfoObject();

	if (planItemObject['PlanName'] == '' || planItemObject['PlanDescription'] == '' || planItemObject['PlanDifficulty'] == '-1'){
				openNotify('warning','Please fill all required (*) information');
	}
	else{
		$.ajax(
			{  
	           url: '/plan/save',  
	           type: 'POST',
	           data: {plan : JSON.stringify(planItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getPlanGridData();

	           			openNotify('success','Save operation succeeded.');	

	           			$('#planItemInfoModal').modal('hide');
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getPlanGridData(){
	$('#planItemInfoModal').modal('hide');
	
	$.ajax(
		{  
           url: '/plan/getPlanGridData',  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tablePlanList tbody').html(result);
           }  
       }
    );
}

function clearPlanItemInfoModal(){
	$('#planIdPlanItemInfo').val('');
	$('#planNamePlanItemInfo').val('');
	$('#planDescriptionPlanItemInfo').val('');
	$('#planDifficultyPlanItemInfo').val('-1');	
}

function getPlanItemInfoObject(){
	var obj =  {		
		PlanName : $('#planNamePlanItemInfo').val().trim(),
		PlanDescription : $('#planDescriptionPlanItemInfo').val().trim(),
		PlanDifficulty : $('#planDifficultyPlanItemInfo').val().trim()
	};

	var id = $('#planIdPlanItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}

function setPlanItemInfoModalObject(tr){
	$('#planIdPlanItemInfo').val(tr.find('.rowDataPlanId').html());
	$('#planNamePlanItemInfo').val(tr.find('.rowDataPlanName').html());
	$('#planDescriptionPlanItemInfo').val(tr.find('.rowDataPlanDescription').html());
	$('#planDifficultyPlanItemInfo').val(tr.find('.rowDataPlanDifficulty').html());	
}


