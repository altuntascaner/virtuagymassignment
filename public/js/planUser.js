


$(document).on('click','#addPlanUserItem',function(){

	clearPlanUserItemInfoModal();
	
	$('#planUserItemInfoModal .modal-title').html('Add New Plan User');

	$('#planUserItemInfoModal').modal('show');
});

$(document).on('click','.editPlanUserItem',function(){

	clearPlanUserItemInfoModal();
	
	var tr = $(this).closest('tr');

	setPlanUserItemInfoModalObject(tr);

	$('#planUserItemInfoModal .modal-title').html('Edit Plan User');

	$('#planUserItemInfoModal').modal('show');
});

$(document).on('click','.removePlanUserItem',function(){
	
	var tr = $(this).closest('tr');
	var userId = tr.find('.rowDataPlanUserId').html();
	var planId = $('#currentPlanId').val();

    openGeneralRemoveConfirmModal('/planUser/remove/'+userId,'getPlanUserGridData(\''+planId+'\')');
});

$('#planUserItemSave').on('click',function(){
	var planUserItemObject = getPlanUserItemInfoObject();

	if (planUserItemObject['UserId'] == ''){
				openNotify('warning','Please fill all required (*) information');
	}
	else{
		$.ajax(
			{  
	           url: '/planUser/save',  
	           type: 'POST',
	           data: {planUser : JSON.stringify(planUserItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getPlanUserGridData(planUserItemObject['PlanId']);

	           			openNotify('success','Save operation succeeded.');

	           			$('#planUserItemInfoModal').modal('hide');	
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getPlanUserGridData(id){
	$('#planUserItemInfoModal').modal('hide');	

	$.ajax(
		{  
           url: '/planUser/getPlanUserGridData/'+id,  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tablePlanUsers tbody').html(result);
           }  
       }
    );
}

function clearPlanUserItemInfoModal(){
	$('#planUserUserIdPlanUserItemInfo').val('');
	$('#planUserUserNamePlanUserItemInfo').val('-1');
}

function getPlanUserItemInfoObject(){
	var obj =  {
		PlanId : $('#currentPlanId').val(),		
		UserId : $('#planUserUserNamePlanUserItemInfo').val().trim()
	};

	var id = $('#planUserUserIdPlanUserItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}


