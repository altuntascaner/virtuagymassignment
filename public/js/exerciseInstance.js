


$(document).on('click','#addExerciseInstanceItem',function(){

	clearExerciseInstanceItemInfoModal();
	
	$('#exerciseInstanceItemInfoModal .modal-title').html('Add New Exercise Instance');

	$('#exerciseInstanceItemInfoModal').modal('show');
});

$(document).on('click','.editExerciseInstanceItem',function(){

	clearExerciseInstanceItemInfoModal();
	
	var tr = $(this).closest('tr');

	setExerciseInstanceItemInfoModalObject(tr);

	$('#exerciseInstanceItemInfoModal .modal-title').html('Edit Exercise Instance');

	$('#exerciseInstanceItemInfoModal').modal('show');
});

$(document).on('click','.removeExerciseInstanceItem',function(){
	
	var tr = $(this).closest('tr');
	var instanceId = tr.find('.rowDataExerciseInstanceId').html()
	var dayId = $('#currentPlanDayId').val();
    openGeneralRemoveConfirmModal('/exerciseInstance/remove/'+instanceId,'getExerciseInstanceGridData(\''+dayId+'\')');
});

$('#exerciseInstanceItemSave').on('click',function(){
	var exerciseInstanceItemObject = getExerciseInstanceItemInfoObject();

	if (exerciseInstanceItemObject['ExerciseId'] == '-1'  ){
				openNotify('warning','Please fill all required (*) information');
	}
	else if(exerciseInstanceItemObject['ExerciseDuration'] == '' || !isPositiveNumber(exerciseInstanceItemObject['ExerciseDuration']) || parseInt(exerciseInstanceItemObject['ExerciseDuration']) > 999999999){
		openNotify('warning','\'Duration\' is out of limit');
	}
	 else if(exerciseInstanceItemObject['ExerciseOrder'] == '' || !isPositiveNumber(exerciseInstanceItemObject['ExerciseOrder']) || parseInt(exerciseInstanceItemObject['ExerciseOrder']) > 999999999){
		openNotify('warning','\'Order\' is out of limit');
	}
	else{
		$.ajax(
			{  
	           url: '/exerciseInstance/save',  
	           type: 'POST',
	           data: {exerciseInstance : JSON.stringify(exerciseInstanceItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getExerciseInstanceGridData(exerciseInstanceItemObject['DayId']);

	           			openNotify('success','Save operation succeeded.');

	           			$('#exerciseInstanceItemInfoModal').modal('hide');	
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getExerciseInstanceGridData(id){
	$('#exerciseInstanceItemInfoModal').modal('hide');	
	
	$.ajax(
		{  						   
           url: '/exerciseInstance/getExerciseInstanceGridData/'+id,  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tableExerciseInstances tbody').html(result);
           }  
       }
    );
}

function clearExerciseInstanceItemInfoModal(){
	$('#exerciseInstanceIdExerciseInstanceItemInfo').val('');
	$('#exerciseInstanceExerciseExerciseInstanceItemInfo').val('-1');
	$('#exerciseInstanceDurationExerciseInstanceItemInfo').val('');
	$('#exerciseInstanceOrderExerciseInstanceItemInfo').val('');
}

function getExerciseInstanceItemInfoObject(){
	var obj =  {
		DayId : $('#currentPlanDayId').val(),		
		ExerciseId : $('#exerciseInstanceExerciseExerciseInstanceItemInfo').val().trim(),
		ExerciseDuration : $('#exerciseInstanceDurationExerciseInstanceItemInfo').val().trim(),
		ExerciseOrder: $('#exerciseInstanceOrderExerciseInstanceItemInfo').val().trim(),

	};

	var id = $('#exerciseInstanceIdExerciseInstanceItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}

function setExerciseInstanceItemInfoModalObject(tr){
	$('#exerciseInstanceIdExerciseInstanceItemInfo').val(tr.find('.rowDataExerciseInstanceId').html());
	$('#exerciseInstanceExerciseExerciseInstanceItemInfo').val(tr.find('.rowDataExerciseInstanceExerciseId').html());
	$('#exerciseInstanceOrderExerciseInstanceItemInfo').val(tr.find('.rowDataExerciseInstanceOrder').html());
	$('#exerciseInstanceDurationExerciseInstanceItemInfo').val(tr.find('.rowDataExerciseInstanceDuration').html());	
}


