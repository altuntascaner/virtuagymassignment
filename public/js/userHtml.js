
getUserGridData();

$(document).on('click','#addUserItem',function(){

	clearUserItemInfoModal();
	
	$('#userItemInfoModal .modal-title').html('Add New User');

	$('#userItemInfoModal').modal('show');
});

$(document).on('click','.editUserItem',function(){

	clearUserItemInfoModal();
	
	var tr = $(this).closest('tr');

	setUserItemInfoModalObject(tr);

	$('#userItemInfoModal .modal-title').html('Edit User');

	$('#userItemInfoModal').modal('show');
});

$(document).on('click','.removeUserItem',function(){
	
	var tr = $(this).closest('tr');
	var id = tr.find('td:eq(0)').html();

    openGeneralRemoveConfirmModal('/user/remove/'+id,'getUserGridData()');
});

$('#userItemSave').on('click',function(){
	var userItemObject = getUserItemInfoObject();

	if (userItemObject['FirstName'] == '' || userItemObject['LastName'] == '' || userItemObject['EMail'] == '-1'){
		openNotify('warning','Please fill all required (*) information');
	}
	else if(!validateEmail(userItemObject['Email'])){
		openNotify('warning','Please enter a valid email');
	}
	else{
		$.ajax(
			{  
	           url: '/user/save',  
	           type: 'POST',
	           data: {user : JSON.stringify(userItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getUserGridData();

	           			openNotify('success','Save operation succeeded.');

	           			$('#userItemInfoModal').modal('hide');	
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getUserGridData(){
	$('#userItemInfoModal').modal('hide');
	
	$.ajax(
		{  
           url: '/user/getUserGridData',  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tableUserList tbody').html(result);
           }  
       }
    );
}

function clearUserItemInfoModal(){
	$('#userIdUserItemInfo').val('');
	$('#userFirstNameUserItemInfo').val('');
	$('#userLastNameUserItemInfo').val('');
	$('#userEmailUserItemInfo').val('');	
}

function getUserItemInfoObject(){
	var obj =  {		
		FirstName : $('#userFirstNameUserItemInfo').val().trim(),
		LastName : $('#userLastNameUserItemInfo').val().trim(),
		Email : $('#userEmailUserItemInfo').val().trim()
	};

	var id = $('#userIdUserItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}

function setUserItemInfoModalObject(tr){
	$('#userIdUserItemInfo').val(tr.find('.rowDataUserId').html());
	$('#userFirstNameUserItemInfo').val(tr.find('.rowDataUserFirstName').html());
	$('#userLastNameUserItemInfo').val(tr.find('.rowDataUserLastName').html());
	$('#userEmailUserItemInfo').val(tr.find('.rowDataUserEmail').html());	
}

function validateEmail(email) 
{
    var re = /[^@]+@[^@]+\.[^@]+/;
    return re.test(email) && email.replace(/[^@]/g,'').length == 1;
}

