
getExerciseGridData();

$(document).on('click','#addExerciseItem',function(){

	clearExerciseItemInfoModal();
	
	$('#exerciseItemInfoModal .modal-title').html('Add New Exercise');

	$('#exerciseItemInfoModal').modal('show');
});

$(document).on('click','.editExerciseItem',function(){

	clearExerciseItemInfoModal();
	
	var tr = $(this).closest('tr');

	setExerciseItemInfoModalObject(tr);

	$('#exerciseItemInfoModal .modal-title').html('Edit Exercise');

	$('#exerciseItemInfoModal').modal('show');
});

$(document).on('click','.removeExerciseItem',function(){
	
	var tr = $(this).closest('tr');
	var id = tr.find('td:eq(0)').html();

    openGeneralRemoveConfirmModal('/exercise/remove/'+id,'getExerciseGridData();');
});

$('#exerciseItemSave').on('click',function(){
	var exerciseItemObject = getExerciseItemInfoObject();

	if (exerciseItemObject['ExerciseName'] == ''){
				openNotify('warning','Please fill all required (*) information');
	}
	else{
		$.ajax(
			{  
	           url: '/exercise/save',  
	           type: 'POST',
	           data: {exercise : JSON.stringify(exerciseItemObject)},
	           dataType: 'text',  
	           async: true,
	           success: function(result) {  
	           		
	           		if(result == "success"){
	           			getExerciseGridData();

	           			openNotify('success','Save operation succeeded.');

	           			$('#exerciseItemInfoModal').modal('hide');	
	           		}
	           		else{
	           			openNotify('danger','Save operation failed.');
	           		}
	           }  
           }
        );
    }

});

function getExerciseGridData(){
	$('#exerciseItemInfoModal').modal('hide');

	$.ajax(
		{  
           url: '/exercise/getExerciseGridData',  
           type: 'POST',
           dataType: 'text',  
           async: true,
           success: function(result) {  
           		
           		$('#tableExerciseList tbody').html(result);
           }  
       }
    );
}

function clearExerciseItemInfoModal(){
	$('#exerciseIdExerciseItemInfo').val('');
	$('#exerciseNameExerciseItemInfo').val('');
}

function getExerciseItemInfoObject(){
	var obj =  {		
		ExerciseName : $('#exerciseNameExerciseItemInfo').val().trim()
	};

	var id = $('#exerciseIdExerciseItemInfo').val().trim();

	if(id != ''){
		obj['id'] = id
	}

	return obj;
}

function setExerciseItemInfoModalObject(tr){
	$('#exerciseIdExerciseItemInfo').val(tr.find('.rowDataExerciseId').html());
	$('#exerciseNameExerciseItemInfo').val(tr.find('.rowDataExerciseName').html());
}



